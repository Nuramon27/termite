use std::cmp::Ordering;
use std::collections::HashSet;

use num_enum::IntoPrimitive;

use termite::graph::parse::parse;
use termite::graph::parse::custom::*;
use termite::shared_node::{SharedCalc, SharedNode};
use termite::{Side, LEFT, RIGHT};

#[derive(Debug, Clone)]
enum OperatorStringBool {
    Bool(SharedNode<'static, bool>),
    Str(SharedNode<'static, String>),
}

impl AnyOperator<'static> for OperatorStringBool {
    type Out = bool;
    fn to_calc(&self) -> SharedCalc<'static> {
        match self {
            OperatorStringBool::Bool(node) => SharedNode::to_calc(node),
            OperatorStringBool::Str(node) => SharedNode::to_calc(node),
        }
    }
    fn to_final(&self) -> Result<SharedNode<'static, Self::Out>, Error> {
        match self {
            OperatorStringBool::Bool(op) => Ok(op.clone()),
            OperatorStringBool::Str(_op) => Err(Error::WrongFinalType {
                got: "String".to_string(),
                exp: "bool".to_string(),
            }),
        }
    }
}

#[derive(Debug)]
enum PreOperatorStringBool {
    StringString(Multifix<'static, String, String>),
    StringBool(Multifix<'static, String, bool>),
    BoolBool(Multifix<'static, bool, bool>),
}

impl From<Multifix<'static, String, String>> for PreOperatorStringBool {
    fn from(other: Multifix<'static, String, String>) -> Self {
        PreOperatorStringBool::StringString(other)
    }
}

impl From<Multifix<'static, String, bool>> for PreOperatorStringBool {
    fn from(other: Multifix<'static, String, bool>) -> Self {
        PreOperatorStringBool::StringBool(other)
    }
}

impl From<Multifix<'static, bool, bool>> for PreOperatorStringBool {
    fn from(other: Multifix<'static, bool, bool>) -> Self {
        PreOperatorStringBool::BoolBool(other)
    }
}

impl PreAnyOperator<'static> for PreOperatorStringBool {
    type In = String;
    type Final = OperatorStringBool;
    fn edge(&mut self, parent: &Self::Final, side: Side) -> Result<(), Error> {
        match self {
            PreOperatorStringBool::StringString(op) => match parent {
                OperatorStringBool::Bool(_) => Err(Error::WrongOperandType {
                    got: "bool".to_string(),
                    exp: "String".to_string(),
                    side,
                }),
                OperatorStringBool::Str(parent) => op.edge(parent.clone(), side),
            },
            PreOperatorStringBool::StringBool(op) => match parent {
                OperatorStringBool::Bool(_) => Err(Error::WrongOperandType {
                    got: "bool".to_string(),
                    exp: "String".to_string(),
                    side,
                }),
                OperatorStringBool::Str(parent) => op.edge(parent.clone(), side),
            },
            PreOperatorStringBool::BoolBool(op) => match parent {
                OperatorStringBool::Bool(parent) => op.edge(parent.clone(), side),
                OperatorStringBool::Str(_) => Err(Error::WrongOperandType {
                    got: "String".to_string(),
                    exp: "bool".to_string(),
                    side,
                }),
            },
        }
    }
    fn edge_with_input(
        &mut self,
        parent: SharedNode<'static, Self::In>,
        side: Side,
    ) -> Result<(), Error> {
        match self {
            PreOperatorStringBool::StringString(op) => op.edge(parent, side),
            PreOperatorStringBool::StringBool(op) => op.edge(parent, side),
            PreOperatorStringBool::BoolBool(_) => Err(Error::WrongOperandType {
                got: "String".to_string(),
                exp: "bool".to_string(),
                side,
            }),
        }
    }
    fn try_finalize(self) -> Result<Self::Final, (Self, Error)> {
        match self {
            PreOperatorStringBool::BoolBool(op) => op
                .try_finalize()
                .map(OperatorStringBool::Bool)
                .map_err(|(s, e)| (PreOperatorStringBool::BoolBool(s), e)),
            PreOperatorStringBool::StringBool(op) => op
                .try_finalize()
                .map(OperatorStringBool::Bool)
                .map_err(|(s, e)| (PreOperatorStringBool::StringBool(s), e)),
            PreOperatorStringBool::StringString(_) => Err((
                self,
                Error::WrongFinalType {
                    got: "String".to_string(),
                    exp: "bool".to_string(),
                },
            )),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, IntoPrimitive)]
#[repr(u8)]
enum ArithmeticPrecedence {
    Zero = 0x0,
    BooleanOr = 0x1,
    BooleanAnd = 0x2,
    BooleanPrefix = 0x3,
    Comparison = 0x4,
}

impl PartialOrd<Self> for ArithmeticPrecedence {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for ArithmeticPrecedence {
    fn cmp(&self, other: &Self) -> Ordering {
        u8::from(*self).cmp(&u8::from(*other))
    }
}

impl Fix for ArithmeticPrecedence {
    const ZERO: Self = ArithmeticPrecedence::Zero;
    fn position(&self) -> Position {
        match self {
            ArithmeticPrecedence::Zero => Position::PREFIX,
            ArithmeticPrecedence::BooleanPrefix => Position::PREFIX,
            ArithmeticPrecedence::BooleanOr
            | ArithmeticPrecedence::BooleanAnd
            | ArithmeticPrecedence::Comparison => Position::INFIX,
        }
    }
    fn first(&self) -> Side {
        match self {
            ArithmeticPrecedence::Zero => LEFT,
            ArithmeticPrecedence::BooleanPrefix => RIGHT,
            ArithmeticPrecedence::BooleanOr
            | ArithmeticPrecedence::BooleanAnd
            | ArithmeticPrecedence::Comparison => LEFT,
        }
    }
}

fn get_operand(
    s: &mut &str,
    variables: &HashSet<String>,
) -> Result<Option<PreNode<ArithmeticPrecedence, OperatorStringBool, PreOperatorStringBool>>, Error> {
    if s.starts_with('(') {
        *s = &s["(".len()..];
        Ok(Some(PreNode::BracketOpen))
    } else if s.starts_with(')') {
        *s = &s[")".len()..];
        Ok(Some(PreNode::BracketClose))
    } else if s.starts_with("==") {
        *s = &s["==".len()..];
        Ok(Some(PreNode::PreOperator(PreOperator {
            precedence: ArithmeticPrecedence::Comparison,
            ty: Multifix::<String, bool>::infix(Box::new(|a, b| a == b)).into(),
        })))
    } else if s.starts_with("!=") {
        *s = &s["!=".len()..];
        Ok(Some(PreNode::PreOperator(PreOperator {
            precedence: ArithmeticPrecedence::Comparison,
            ty: Multifix::<String, bool>::infix(Box::new(|a, b| a != b)).into(),
        })))
    } else if s.starts_with(">") {
        *s = &s[">".len()..];
        Ok(Some(PreNode::PreOperator(PreOperator {
            precedence: ArithmeticPrecedence::Comparison,
            ty: Multifix::<String, bool>::infix(Box::new(|a, b| a.contains(b))).into(),
        })))
    } else if s.starts_with("&&") {
        *s = &s["&&".len()..];
        Ok(Some(PreNode::PreOperator(PreOperator {
            precedence: ArithmeticPrecedence::BooleanAnd,
            ty: Multifix::<bool, bool>::infix(Box::new(|a, b| *a && *b)).into(),
        })))
    } else if s.starts_with("||") {
        *s = &s["||".len()..];
        Ok(Some(PreNode::PreOperator(PreOperator {
            precedence: ArithmeticPrecedence::BooleanOr,
            ty: Multifix::<bool, bool>::infix(Box::new(|a, b| *a || *b)).into(),
        })))
    } else if s.starts_with("!") {
        *s = &s["!".len()..];
        Ok(Some(PreNode::PreOperator(PreOperator {
            precedence: ArithmeticPrecedence::BooleanPrefix,
            ty: Multifix::<bool, bool>::prefix(Box::new(|a: &bool| !a)).into(),
        })))
    } else if s.starts_with(' ') {
        *s = s.trim_start();
        Ok(None)
    } else if let Some(varname) = variables.iter().find(|varname| s.starts_with(*varname)) {
        *s = &s[varname.len()..];
        Ok(Some(PreNode::Variable {
            name: varname.to_string(),
        }))
    } else {
        Err(Error::InvalidOperator(s.to_string()))
    }
}
#[test]
fn parse_simple() {
    let variables: HashSet<String> = ["a", "b"].iter().map(|s| s.to_string()).collect();
    let mut term = "a == b";

    let mut graph =
        parse(&mut term, |s| get_operand(s, &variables)).unwrap_or_else(|e| panic!("{:?}", e));
    graph.set_variable("a", "Ich".to_string());
    graph.set_variable("b", "Du".to_string());

    assert_eq!(graph.calc().unwrap(), false);
}
#[test]
fn parse_multiple_receivers() {
    let variables: HashSet<String> = ["a", "b"].iter().map(|s| s.to_string()).collect();
    let mut term = "a == a";

    let mut graph =
        parse(&mut term, |s| get_operand(s, &variables)).unwrap_or_else(|e| panic!("{:?}", e));
    graph.set_variable("a", "Ich".to_string());
    graph.set_variable("b", "Du".to_string());

    assert_eq!(graph.calc().unwrap(), true);
}
#[test]
fn parse_complex() {
    let variables: HashSet<String> = ["a", "b", "c"].iter().map(|s| s.to_string()).collect();
    let mut term = "a == b && a != c";

    let mut graph =
        parse(&mut term, |s| get_operand(s, &variables)).unwrap_or_else(|e| panic!("{:?}", e));
    graph.set_variable("a", "Ich".to_string());
    graph.set_variable("b", "Ich".to_string());
    graph.set_variable("c", "Du".to_string());

    assert_eq!(graph.calc().unwrap(), true);
}
#[test]
fn parse_brackets() {
    let variables: HashSet<String> = ["a", "b", "c"].iter().map(|s| s.to_string()).collect();
    let mut term = "(((a != b))) && (a == c || b == (c))";

    let mut graph =
        parse(&mut term, |s| get_operand(s, &variables)).unwrap_or_else(|e| panic!("{:?}", e));
    graph.set_variable("a", "Ich".to_string());
    graph.set_variable("b", "Du".to_string());
    graph.set_variable("c", "Du".to_string());

    assert_eq!(graph.calc().unwrap(), true);
}
#[test]
fn parse_everything_in_bracket() {
    let variables: HashSet<String> = ["a", "b"].iter().map(|s| s.to_string()).collect();
    let mut term = "(((a != b)))";

    let mut graph =
        parse(&mut term, |s| get_operand(s, &variables)).unwrap_or_else(|e| panic!("{:?}", e));
    graph.set_variable("a", "Ich".to_string());
    graph.set_variable("b", "Du".to_string());

    assert_eq!(graph.calc().unwrap(), true);
}

#[test]
fn parse_postfix() {
    let variables: HashSet<String> = ["a", "b"].iter().map(|s| s.to_string()).collect();
    let mut term = "! a == b";

    let mut graph =
        parse(&mut term, |s| get_operand(s, &variables)).unwrap_or_else(|e| panic!("{:?}", e));
    graph.set_variable("a", "Ich".to_string());
    graph.set_variable("b", "Du".to_string());

    assert_eq!(graph.calc().unwrap(), true);
}

#[test]
fn parse_postfix_precedence() {
    let variables: HashSet<String> = ["a", "b"].iter().map(|s| s.to_string()).collect();
    let mut term = "a != b || !! a == b";

    let mut graph =
        parse(&mut term, |s| get_operand(s, &variables)).unwrap_or_else(|e| panic!("{:?}", e));
    graph.set_variable("a", "Ich".to_string());
    graph.set_variable("b", "Ich".to_string());

    assert_eq!(graph.calc().unwrap(), true);
}

#[test]
fn parse_asymmetric_infix() {
    let variables: HashSet<String> = ["a", "b"].iter().map(|s| s.to_string()).collect();
    let mut term = "a > b";

    let mut graph =
        parse(&mut term, |s| get_operand(s, &variables)).unwrap_or_else(|e| panic!("{:?}", e));
    graph.set_variable("a", "Halbmann".to_string());
    graph.set_variable("b", "mann".to_string());

    assert_eq!(graph.calc().unwrap(), true);
}