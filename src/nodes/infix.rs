use std::any;
use std::fmt;
use std::ops::Deref;

use crate::shared_node::{RefCalc, SharedCalc, SharedNode, SharedNodeTyped};
use crate::{to_calc_iter, Calc, Edge, Node};

use crate::apply_to_operand;

pub struct InfixBuilder<In, Out = In> {
    func: Box<dyn Fn(&In, &In) -> Out>,
}

impl<In, Out> fmt::Debug for InfixBuilder<In, Out> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "InfixBuilder<{}, {}>{{ func: {:?} }}",
            any::type_name::<In>(),
            any::type_name::<Out>(),
            self.func.as_ref() as *const _
        )
    }
}

impl<In, Out> InfixBuilder<In, Out> {
    pub fn new(func: Box<dyn Fn(&In, &In) -> Out>) -> Self {
        Self { func }
    }
    pub fn with_operands<'a, 'r>(
        self,
        left: &'a SharedNode<'r, In>,
        right: &'a SharedNode<'r, In>,
    ) -> SharedNodeTyped<'r, Infix<'r, In, Out>>
    where
        In: 'r,
        Out: 'r,
    {
        let left_edge = left.borrow().target().clone();
        let right_edge = right.borrow().target().clone();

        let node = SharedNodeTyped::new(Infix {
            left: left_edge,
            right: right_edge,
            res: Edge::default(),
            receivers: Vec::new(),
            func: self.func,
        });
        left.borrow_mut()
            .add_receiver(SharedNodeTyped::to_calc(&node));
        right
            .borrow_mut()
            .add_receiver(SharedNodeTyped::to_calc(&node));

        node
    }
}

pub struct Infix<'r, In: 'r, Out: 'r = In> {
    left: Edge<In>,
    right: Edge<In>,
    res: Edge<Out>,
    receivers: Vec<SharedCalc<'r>>,
    func: Box<dyn Fn(&In, &In) -> Out>,
}

impl<'r, In: 'r, Out: 'r> fmt::Debug for Infix<'r, In, Out> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "Infix<{}, {}>{{ receivers: ",
            any::type_name::<In>(),
            any::type_name::<Out>()
        )?;
        for rec in &self.receivers {
            write!(f, "{:?}, ", rec.borrow().deref().id())?;
        }
        write!(f, "}}")
    }
}

impl<'r, In: 'r, Out: 'r> Calc<'r> for Infix<'r, In, Out> {
    fn id(&self) -> usize {
        self.receivers.as_ptr() as usize
    }
    fn calc(&self) -> Result<(), ()> {
        let value = apply_to_operand!(self.func.as_ref(), &self.left, &self.right);
        self.res.assign(value);
        Ok(())
    }
    fn receivers<'a>(&'a self) -> Box<dyn Iterator<Item = RefCalc<'a, 'r>> + 'a> {
        to_calc_iter(&self.receivers)
    }
}

impl<'r, In: 'r, Out: 'r> Node<'r> for Infix<'r, In, Out> {
    //type In = In;
    type Res = Out;
    fn add_receiver(&mut self, receiver: SharedCalc<'r>) {
        self.receivers.push(receiver);
    }
    fn target(&self) -> &Edge<Self::Res> {
        &self.res
    }
}
