use std::any;
use std::fmt;
use std::ops::Deref;

use crate::shared_node::{RefCalc, SharedCalc};
use crate::{to_calc_iter, Calc, Edge, Node};

pub struct Constant<'r, T: 'r> {
    value: Edge<T>,
    receivers: Vec<SharedCalc<'r>>,
}

impl<'r, T: 'r> Constant<'r, T> {
    pub fn new(value: T) -> Self {
        Constant {
            value: Edge::new(value),
            receivers: Vec::new(),
        }
    }
}

impl<'r, T: 'r> fmt::Debug for Constant<'r, T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "Constant<{}> {{ id: {:?}, receivers: [",
            any::type_name::<T>(),
            self.id()
        )?;
        for rec in &self.receivers {
            write!(f, "{:?}, ", rec.borrow().deref().id())?;
        }
        write!(f, "}}")
    }
}

impl<'r, T: 'r> Calc<'r> for Constant<'r, T> {
    fn id(&self) -> usize {
        self.receivers.as_ptr() as usize
    }
    fn calc(&self) -> Result<(), ()> {
        Ok(())
    }
    fn receivers<'a>(&'a self) -> Box<dyn Iterator<Item = RefCalc<'a, 'r>> + 'a> {
        to_calc_iter(&self.receivers)
    }
}

impl<'r, T: 'r> Node<'r> for Constant<'r, T> {
    type Res = T;
    //type In = Infallible;
    fn add_receiver(&mut self, receiver: SharedCalc<'r>) {
        self.receivers.push(receiver);
    }
    fn target(&self) -> &Edge<Self::Res> {
        &self.value
    }
}

pub struct Variable<'r, T: 'r> {
    value: Edge<T>,
    receivers: Vec<SharedCalc<'r>>,
}

impl<'r, T: 'r> Default for Variable<'r, T> {
    fn default() -> Self {
        Self {
            value: Edge::default(),
            receivers: Vec::new(),
        }
    }
}

impl<'r, T: 'r> fmt::Debug for Variable<'r, T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "Variable<{}> {{ id: {:?}, receivers: [",
            any::type_name::<T>(),
            self.id()
        )?;
        for rec in &self.receivers {
            write!(f, "{:?}, ", rec.borrow().deref().id())?;
        }
        write!(f, "}}")
    }
}

impl<'r, T: 'r> Variable<'r, T> {
    pub fn new() -> Self {
        Self::default()
    }
}

impl<'r, T: 'r> Variable<'r, T> {
    pub fn set_value(&self, value: T) {
        self.value.assign(value);
    }
    /// # Panics
    ///
    /// If there is no value currently set.
    pub fn assign_with<F: FnOnce(&mut Option<T>)>(&self, setter: F) {
        self.value.assign_with(setter)
    }
}

impl<'r, T: 'r> Calc<'r> for Variable<'r, T> {
    fn id(&self) -> usize {
        self.receivers.as_ptr() as usize
    }
    fn calc(&self) -> Result<(), ()> {
        Ok(())
    }
    fn receivers<'a>(&'a self) -> Box<dyn Iterator<Item = RefCalc<'a, 'r>> + 'a> {
        to_calc_iter(&self.receivers)
    }
}

impl<'r, T: 'r> Node<'r> for Variable<'r, T> {
    //type In = Infallible;
    type Res = T;
    fn add_receiver(&mut self, receiver: SharedCalc<'r>) {
        self.receivers.push(receiver);
    }
    fn target(&self) -> &Edge<Self::Res> {
        &self.value
    }
}
