use std::any;
use std::cell::Ref;
use std::fmt;
use std::ops::Deref;

use crate::apply_to_operand;
use crate::shared_node::{RefCalc, SharedCalc, SharedNode, SharedNodeTyped};
use crate::{to_calc_iter, Calc, Edge, Node};

pub struct Identity<'r, In: 'r> {
    op: Edge<In>,
    receivers: Vec<SharedCalc<'r>>,
}

impl<'r, In: 'r> fmt::Debug for Identity<'r, In> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Identity<{}>{{ receivers: ", any::type_name::<In>())?;
        for rec in &self.receivers {
            write!(f, "{:?}, ", rec.borrow().deref().id())?;
        }
        write!(f, "}}")
    }
}

impl<'r, In: 'r> Identity<'r, In> {
    pub fn with_operand(parent: &SharedNode<'r, In>) -> SharedNodeTyped<'r, Self> {
        let edge = parent.borrow().target().clone();
        let res = SharedNodeTyped::new(Identity {
            op: edge,
            receivers: Vec::new(),
        });
        parent
            .borrow_mut()
            .add_receiver(SharedNodeTyped::to_calc(&res));
        res
    }
    pub fn value(&self) -> Option<Ref<In>> {
        self.op.value()
    }
}

impl<'r, In: 'r> Calc<'r> for Identity<'r, In> {
    fn id(&self) -> usize {
        self.receivers.as_ptr() as usize
    }
    fn calc(&self) -> Result<(), ()> {
        Ok(())
    }
    fn receivers<'a>(&'a self) -> Box<dyn Iterator<Item = RefCalc<'a, 'r>> + 'a> {
        to_calc_iter(&self.receivers)
    }
}

impl<'r, In: 'r> Node<'r> for Identity<'r, In> {
    //type In = In;
    type Res = In;
    fn add_receiver(&mut self, receiver: SharedCalc<'r>) {
        self.receivers.push(receiver);
    }
    fn target(&self) -> &Edge<Self::Res> {
        &self.op
    }
}

pub struct MonadBuilder<In, Out = In> {
    func: Box<dyn Fn(&In) -> Out>,
}

impl<In, Out> fmt::Debug for MonadBuilder<In, Out> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "MonadBuilder<{}, {}>{{ func: {:?} }}",
            any::type_name::<In>(),
            any::type_name::<Out>(),
            self.func.as_ref() as *const _
        )
    }
}

impl<In: Clone> MonadBuilder<In, In> {
    pub fn identity() -> Self {
        Self {
            func: Box::new(|x| x.clone()),
        }
    }
}

impl<In, Out> MonadBuilder<In, Out> {
    pub fn new(func: Box<dyn Fn(&In) -> Out>) -> Self {
        Self { func }
    }
    pub fn with_operands<'r>(
        self,
        op: &mut dyn Node<'r, Res = In>,
    ) -> SharedNodeTyped<'r, Monad<'r, In, Out>>
    where
        In: 'r,
        Out: 'r,
    {
        let edge = op.target().clone();
        let node = SharedNodeTyped::new(Monad {
            op: edge,
            res: Edge::default(),
            receivers: Vec::new(),
            func: self.func,
        });
        op.add_receiver(SharedNodeTyped::to_calc(&node));

        node
    }
}

pub struct Monad<'r, In: 'r, Out: 'r = In> {
    op: Edge<In>,
    res: Edge<Out>,
    receivers: Vec<SharedCalc<'r>>,
    func: Box<dyn Fn(&In) -> Out>,
}

impl<'r, In: 'r, Out: 'r> fmt::Debug for Monad<'r, In, Out> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "Monad<{}, {}>{{ receivers: ",
            any::type_name::<In>(),
            any::type_name::<Out>()
        )?;
        for rec in &self.receivers {
            write!(f, "{:?}, ", rec.borrow().deref().id())?;
        }
        write!(f, "}}")
    }
}

impl<'r, In: 'r, Out: 'r> Calc<'r> for Monad<'r, In, Out> {
    fn id(&self) -> usize {
        self.receivers.as_ptr() as usize
    }
    fn calc(&self) -> Result<(), ()> {
        let value = apply_to_operand!(self.func.as_ref(), &self.op);
        self.res.assign(value);
        Ok(())
    }
    fn receivers<'a>(&'a self) -> Box<dyn Iterator<Item = RefCalc<'a, 'r>> + 'a> {
        to_calc_iter(&self.receivers)
    }
}

impl<'r, In: 'r, Out: 'r> Node<'r> for Monad<'r, In, Out> {
    //type In = In;
    type Res = Out;
    fn add_receiver(&mut self, receiver: SharedCalc<'r>) {
        self.receivers.push(receiver);
    }
    fn target(&self) -> &Edge<Self::Res> {
        &self.res
    }
}
