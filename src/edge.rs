use std::cell::{Ref, RefCell};
use std::rc::Rc;

use std::fmt;

pub struct Edge<T> {
    pub value: Rc<RefCell<Option<T>>>,
}

impl<T> Default for Edge<T> {
    fn default() -> Self {
        Edge {
            value: Rc::new(RefCell::new(None)),
        }
    }
}

impl<T> fmt::Debug for Edge<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:?}", self.value.as_ptr())
    }
}

impl<T> Clone for Edge<T> {
    fn clone(&self) -> Self {
        Self {
            value: self.value.clone(),
        }
    }
}

impl<T> Edge<T> {
    pub fn new(value: T) -> Self {
        Edge {
            value: Rc::new(RefCell::new(Some(value))),
        }
    }
    pub fn assign(&self, value: T) {
        self.value.replace(Some(value));
    }
    /// # Panics
    ///
    /// If there is no value currently set.
    pub fn assign_with<F: FnOnce(&mut Option<T>)>(&self, setter: F) {
        setter(&mut self.value.borrow_mut());
    }
    pub fn value(&self) -> Option<Ref<T>> {
        let value = RefCell::borrow(&self.value);
        if value.is_some() {
            Some(Ref::map(value, |v| v.as_ref().unwrap()))
        } else {
            None
        }
    }
    pub fn take_value(&self) -> Option<T> {
        RefCell::borrow_mut(&self.value).take()
    }
}
