use std::{any, mem};
use std::cell::{Ref, RefCell, RefMut};
use std::fmt;
use std::ops::{Deref, DerefMut};
use std::rc::Rc;

use crate::Node;

use super::{SharedCalc, SharedNode};

pub struct SharedNodeTyped<'r, T: Node<'r>> {
    pub(super) node: *mut T,
    pub(super) rc: Rc<RefCell<()>>,
    pub(super) _mark: std::marker::PhantomData<&'r T>,
}

impl<'r, T: Node<'r>> Drop for SharedNodeTyped<'r, T> {
    fn drop(&mut self) {
        if Rc::strong_count(&self.rc) == 1 {
            // Sound because `self` is the unique owner of `self.node`.
            unsafe { mem::drop(Box::from_raw(self.node)) };
        }
    }
}

impl<'r, T: Node<'r>> fmt::Debug for SharedNodeTyped<'r, T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "SharedNodeTyped<{}>{{ node: {:?} }}",
            any::type_name::<T>(),
            self.node
        )
    }
}

impl<'r, T: Node<'r>> Clone for SharedNodeTyped<'r, T> {
    fn clone(&self) -> Self {
        Self {
            node: self.node,
            rc: self.rc.clone(),
            _mark: std::marker::PhantomData::default(),
        }
    }
}

impl<'r, T: Node<'r>> SharedNodeTyped<'r, T> {
    pub fn new(node: T) -> Self {
        SharedNodeTyped {
            node: Box::into_raw(Box::new(node)),
            rc: Rc::new(RefCell::new(())),
            _mark: std::marker::PhantomData::default(),
        }
    }
    pub fn borrow<'a>(&'a self) -> RefNodeTyped<'a, 'r, T> {
        let borrow = self.rc.borrow();
        RefNodeTyped {
            // Sound because `self.node` is guarded by `borrow`.
            value: unsafe { &*(self.node as *const _) },
            borrow,
            _marker: std::marker::PhantomData::default(),
        }
    }
    pub fn borrow_mut<'a>(&'a self) -> RefMutNodeTyped<'a, 'r, T> {
        let borrow = self.rc.borrow_mut();
        RefMutNodeTyped {
            // Sound because `self.node` is guarded by `borrow`.
            value: unsafe { &mut *(self.node as *mut _) },
            borrow,
            _marker: std::marker::PhantomData::default(),
        }
    }
    pub fn to_calc(this: &SharedNodeTyped<'r, T>) -> SharedCalc<'r> {
        SharedCalc {
            // Sound because `this.node` remains tracked by `rc`.
            calc: unsafe { (&mut *this.node).as_calc_mut() } as *mut _,
            rc: Rc::clone(&this.rc),
        }
    }
    pub fn to_untyped(this: &SharedNodeTyped<'r, T>) -> SharedNode<'r, T::Res> {
        SharedNode {
            node: this.node as *mut _,
            rc: Rc::clone(&this.rc),
        }
    }
    pub fn into_untyped(this: SharedNodeTyped<'r, T>) -> SharedNode<'r, T::Res> {
        SharedNode {
            node: this.node as *mut _,
            rc: this.rc.clone(),
        }
    }
}

#[derive(Debug)]
pub struct RefNodeTyped<'a, 'r, T: Node<'r>> {
    value: &'a T,
    borrow: Ref<'a, ()>,
    _marker: std::marker::PhantomData<&'r T>,
}

impl<'a, 'r, T: Node<'r>> Deref for RefNodeTyped<'a, 'r, T> {
    type Target = T;
    fn deref(&self) -> &Self::Target {
        self.value
    }
}

#[derive(Debug)]
pub struct RefMutNodeTyped<'a, 'r, T: Node<'r>> {
    value: &'a mut T,
    borrow: RefMut<'a, ()>,
    _marker: std::marker::PhantomData<&'r T>,
}

impl<'a, 'r, T: Node<'r>> Deref for RefMutNodeTyped<'a, 'r, T> {
    type Target = T;
    fn deref(&self) -> &Self::Target {
        self.value
    }
}

impl<'a, 'r, T: Node<'r>> DerefMut for RefMutNodeTyped<'a, 'r, T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        self.value
    }
}
