use std::cell::{Ref, RefCell};
use std::{fmt, mem};
use std::ops::Deref;
use std::rc::Rc;

use crate::Calc;

pub struct SharedCalc<'r> {
    pub(super) calc: *mut (dyn Calc<'r> + 'r),
    pub(super) rc: Rc<RefCell<()>>,
}

impl<'r> Drop for SharedCalc<'r> {
    fn drop(&mut self) {
        if Rc::strong_count(&self.rc) == 1 {
            // Sound because `self` is the unique owner of `self.calc`
            unsafe {
                mem::drop(Box::from_raw(self.calc));
            }
        }
    }
}

impl<'r> fmt::Debug for SharedCalc<'r> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "SharedCalc{{ calc: {:?} }}", self.calc)
    }
}

impl<'r> Clone for SharedCalc<'r> {
    fn clone(&self) -> Self {
        Self {
            calc: self.calc,
            rc: self.rc.clone(),
        }
    }
}

impl<'r, T: Calc<'r> + 'r> From<T> for SharedCalc<'r> {
    fn from(other: T) -> Self {
        SharedCalc {
            calc: Box::into_raw(Box::new(other)),
            rc: Rc::new(RefCell::new(())),
        }
    }
}

impl<'r> SharedCalc<'r> {
    pub fn borrow<'a>(&'a self) -> RefCalc<'a, 'r> {
        let borrow = self.rc.borrow();
        RefCalc {
            // Sound because `self.calc` is guarded by `borrow`
            value: unsafe { &*self.calc },
            borrow,
        }
    }
}

#[derive(Debug)]
pub struct RefCalc<'a, 'r> {
    value: &'a (dyn Calc<'r> + 'r),
    borrow: Ref<'a, ()>,
}

impl<'a, 'r> Deref for RefCalc<'a, 'r> {
    type Target = dyn Calc<'r> + 'r;
    fn deref(&self) -> &Self::Target {
        self.value
    }
}
