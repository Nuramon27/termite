use std::{any, mem};
use std::cell::{Ref, RefCell, RefMut};
use std::fmt;
use std::ops::{Deref, DerefMut};
use std::rc::Rc;

use crate::Node;

use super::SharedCalc;

pub struct SharedNode<'r, Res: 'r> {
    pub(super) node: *mut (dyn Node<'r, Res = Res> + 'r),
    pub(super) rc: Rc<RefCell<()>>,
}

impl<'r, Res: 'r> Drop for SharedNode<'r, Res> {
    fn drop(&mut self) {
        if Rc::strong_count(&self.rc) == 1 {
            // Sound because `self` is the unique owner of `self.node`
            unsafe { mem::drop(Box::from_raw(self.node)) };
        }
    }
}

impl<'r, Res: 'r> fmt::Debug for SharedNode<'r, Res> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "SharedNode<{}>{{ node: {:?} }}",
            any::type_name::<Res>(),
            self.node
        )
    }
}

impl<'r, Res: 'r> Clone for SharedNode<'r, Res> {
    fn clone(&self) -> Self {
        SharedNode {
            node: self.node,
            rc: self.rc.clone(),
        }
    }
}

impl<'r, T: Node<'r, Res = Res> + 'r, Res> From<T> for SharedNode<'r, Res> {
    fn from(other: T) -> Self {
        SharedNode {
            node: Box::into_raw(Box::new(other)),
            rc: Rc::new(RefCell::new(())),
        }
    }
}

impl<'r, Res: 'r> SharedNode<'r, Res> {
    pub fn borrow<'a>(&'a self) -> RefNode<'a, 'r, Res> {
        let borrow = self.rc.borrow();
        RefNode {
            // Sound because `self.node` is guarded by `borrow`.
            value: unsafe { &*self.node },
            borrow,
        }
    }
    pub fn borrow_mut<'a>(&'a self) -> RefMutNode<'a, 'r, Res> {
        let borrow = self.rc.borrow_mut();
        RefMutNode {
            // Sound because `self.node` is guarded mutably my `borrow`.
            value: unsafe { &mut *(self.node as *mut _) },
            borrow,
        }
    }
    pub fn to_calc(this: &SharedNode<'r, Res>) -> SharedCalc<'r> {
        SharedCalc {
            // Sound because `this.node` remains tracked by `rc`.
            calc: unsafe { (&mut *this.node).as_calc_mut() } as *mut _,
            rc: Rc::clone(&this.rc),
        }
    }
}

#[derive(Debug)]
pub struct RefNode<'a, 'r, Res: 'r> {
    value: &'a (dyn Node<'r, Res = Res> + 'r),
    borrow: Ref<'a, ()>,
}

impl<'a, 'r, Res: 'r> Deref for RefNode<'a, 'r, Res> {
    type Target = dyn Node<'r, Res = Res> + 'r;
    fn deref(&self) -> &Self::Target {
        self.value
    }
}

#[derive(Debug)]
pub struct RefMutNode<'a, 'r, Res: 'r> {
    value: &'a mut (dyn Node<'r, Res = Res> + 'r),
    borrow: RefMut<'a, ()>,
}

impl<'a, 'r, Res: 'r> Deref for RefMutNode<'a, 'r, Res> {
    type Target = dyn Node<'r, Res = Res> + 'r;
    fn deref(&self) -> &Self::Target {
        self.value
    }
}

impl<'a, 'r, Res: 'r> DerefMut for RefMutNode<'a, 'r, Res> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        self.value
    }
}
