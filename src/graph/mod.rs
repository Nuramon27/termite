use std::any;
use std::borrow::Borrow;
use std::cell::Ref;
use std::collections::HashMap;
use std::fmt;
use std::hash::Hash;
use std::iter::FromIterator;
use std::ops::{Add, AddAssign, Deref};

use crate::nodes::{InfixBuilder, Variable};
use crate::shared_node::{SharedCalc, SharedNodeTyped};
use root::Root;

pub mod parse;
mod root;

pub struct Graph<'r, In: 'r, Out: 'r, VarKey = String>
where
    VarKey: Eq + Hash,
{
    pub(crate) variables: HashMap<VarKey, SharedNodeTyped<'r, Variable<'r, In>>>,
    pub(crate) nodes: Vec<SharedCalc<'r>>,
    pub(crate) root: Root<'r, Out>,
}

impl<'r, In: 'r, Out: 'r, VarKey> fmt::Debug for Graph<'r, In, Out, VarKey>
where
    VarKey: Eq + Hash + fmt::Debug,
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "Graph<{}, {}, {}>{{ variables: {:?}, nodes: {:?}, root: {:?} }}",
            any::type_name::<In>(),
            any::type_name::<Out>(),
            any::type_name::<VarKey>(),
            self.variables,
            self.nodes,
            self.root
        )
    }
}

impl<'r, In: 'r, VarKey: Eq + Hash> From<(VarKey, Variable<'r, In>)> for Graph<'r, In, In, VarKey> {
    fn from((key, var): (VarKey, Variable<'r, In>)) -> Self {
        let var = SharedNodeTyped::new(var);
        let root = Root::new(SharedNodeTyped::to_untyped(&var));
        Graph {
            variables: HashMap::from_iter(std::iter::once((key, var))),
            nodes: Vec::new(),
            root,
        }
    }
}

impl<'r, In: 'r, Out: 'r, VarKey: Eq + Hash> Graph<'r, In, Out, VarKey> {
    fn do_calculations(&self) -> Result<(), ()> {
        for node in &self.nodes {
            node.borrow().deref().calc()?;
        }
        Ok(())
    }
    pub fn calc_ref(&self) -> Option<Ref<Out>> {
        self.do_calculations().ok()?;
        self.root.res()
    }
    pub fn calc_take(&self) -> Option<Out> {
        self.do_calculations().ok()?;
        self.root.take_res()
    }
    pub fn set_variable<Q: ?Sized + Hash + Eq>(&mut self, key: &Q, value: In)
    where
        VarKey: Borrow<Q>,
    {
        if let Some(var) = self.variables.get_mut(key.borrow()) {
            SharedNodeTyped::borrow(&var).set_value(value);
        }
    }
    /// # Panics
    ///
    /// If there is no value currently set to the variable.
    pub fn assign_variable_with<Q: ?Sized + Hash + Eq, F: FnOnce(&mut Option<In>)>(&mut self, key: &Q, setter: F)
    where
        VarKey: Borrow<Q>,
    {
        if let Some(var) = self.variables.get_mut(key.borrow()) {
            SharedNodeTyped::borrow(&var).assign_with(setter);
        }
    }
}

impl<'r, In: 'r, Out: Clone + 'r, VarKey: Eq + Hash> Graph<'r, In, Out, VarKey> {
    pub fn calc_cloned(&self) -> Option<Out> {
        self.do_calculations().ok()?;
        self.root.res().as_deref().cloned()
    }
}

impl<'r, In: 'r, Out: Copy + 'r, VarKey: Eq + Hash> Graph<'r, In, Out, VarKey> {
    pub fn calc(&self) -> Option<Out> {
        self.do_calculations().ok()?;
        self.root.res().as_deref().copied()
    }
}

impl<'r, In: 'r, Out: 'r, VarKey: Eq + Hash> Graph<'r, In, Out, VarKey> {
    fn combine_infix(&mut self, rhs: Graph<'r, In, Out, VarKey>, pre_op: InfixBuilder<Out, Out>) {
        self.variables.extend(rhs.variables);
        self.nodes.extend(rhs.nodes);

        let root = Root::new(SharedNodeTyped::into_untyped(
            pre_op.with_operands(&self.root.node, &rhs.root.node),
        ));
        self.root = root;

        self.nodes.push(self.root.as_calc())
    }
    fn combine_infix_owned<Out2: 'r>(
        mut self,
        rhs: Graph<'r, In, Out, VarKey>,
        pre_op: InfixBuilder<Out, Out2>,
    ) -> Graph<'r, In, Out2, VarKey> {
        self.variables.extend(rhs.variables);
        self.nodes.extend(rhs.nodes);

        let root = Root::new(SharedNodeTyped::into_untyped(
            pre_op.with_operands(&self.root.node, &rhs.root.node),
        ));

        self.nodes.push(root.as_calc());

        Graph {
            variables: self.variables,
            nodes: self.nodes,
            root,
        }
    }
}

impl<'r, In: 'r, Out: 'r, VarKey: Eq + Hash> AddAssign<Graph<'r, In, Out, VarKey>>
    for Graph<'r, In, Out, VarKey>
where
    for<'a, 'c> &'a Out: Add<&'c Out, Output = Out>,
{
    fn add_assign(&mut self, rhs: Graph<'r, In, Out, VarKey>) {
        self.combine_infix(rhs, InfixBuilder::new(Box::new(|a, b| a + b)));
    }
}

impl<'r, In: 'r, Out: 'r, VarKey: Eq + Hash> Graph<'r, In, Out, VarKey>
where
    Out: PartialEq<Out>,
{
    pub fn equals(self, rhs: Graph<'r, In, Out, VarKey>) -> Graph<'r, In, bool, VarKey> {
        self.combine_infix_owned(rhs, InfixBuilder::new(Box::new(|a, b| a == b)))
    }
}

#[cfg(test)]
mod tests {
    use crate::{Graph, Variable};
    #[test]
    fn simple_add() {
        let mut a = Graph::from((0u8, Variable::<f32>::new()));
        let b = Graph::from((1u8, Variable::<f32>::new()));
        a += b;
        a.set_variable(&0, 2.0);
        a.set_variable(&1, 3.0);
        assert_eq!(a.calc_take(), Some(5.0));
    }
}
