use std::any;
use std::cell::Ref;
use std::fmt;

use crate::shared_node::{RefCalc, SharedCalc, SharedNode};
use crate::{Calc, Edge};

pub struct Root<'r, Res: 'r> {
    pub(super) res: Edge<Res>,
    pub(super) node: SharedNode<'r, Res>,
}

impl<'r, Res: 'r> fmt::Debug for Root<'r, Res> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "Root<{}> {{ node: {:?} }}",
            any::type_name::<Res>(),
            self.node
        )
    }
}

impl<'r, Res: 'r> Root<'r, Res> {
    pub fn new(node: SharedNode<'r, Res>) -> Self {
        let edge = node.borrow().target().clone();
        Self { res: edge, node }
    }
    pub fn res(&self) -> Option<Ref<Res>> {
        self.res.value()
    }
    pub(super) fn take_res(&self) -> Option<Res> {
        self.res.take_value()
    }
}

impl<'r, Res: 'r> Root<'r, Res> {
    pub fn as_calc(&self) -> SharedCalc<'r> {
        SharedNode::to_calc(&self.node)
    }
}

impl<'r, Res: 'r> Calc<'r> for Root<'r, Res> {
    fn id(&self) -> usize {
        self.node.borrow().id()
    }
    fn calc(&self) -> Result<(), ()> {
        self.res.value().as_ref().map(|_| ()).ok_or(())
    }
    fn receivers<'a>(&'a self) -> Box<dyn Iterator<Item = RefCalc<'a, 'r>> + 'a> {
        Box::new(std::iter::empty())
    }
}
