use std::cell::RefCell;
use std::rc::{Rc, Weak};

use std::fmt;

use crate::nodes::Variable;
use crate::shared_node::SharedNodeTyped;

use super::custom::{AnyOperator, Error, Fix, PreAnyOperator, PreOperator};

#[derive(Debug, Clone)]
pub enum PreNodePrivate<
    'r,
    In: 'r,
    Precedence: Fix,
    OperatorType: AnyOperator<'r>,
    PreOperatorType: PreAnyOperator<'r, In = In, Final = OperatorType>,
> {
    /// Only for temporary purposes
    Nothing,
    Constant(OperatorType),
    Variable(String, SharedNodeTyped<'r, Variable<'r, In>>),
    BracketIn(
        Rc<RefCell<Option<PreNodePrivate<'r, In, Precedence, OperatorType, PreOperatorType>>>>,
    ),
    BracketOut(
        Weak<RefCell<Option<PreNodePrivate<'r, In, Precedence, OperatorType, PreOperatorType>>>>,
    ),
    OperatorReady(OperatorType),
    OperatorAwaiting(PreOperator<Precedence, PreOperatorType>),
}

impl<
        'r,
        In: 'r + fmt::Debug,
        Precedence: Fix + fmt::Debug,
        OperatorType: AnyOperator<'r> + fmt::Debug,
        PreOperatorType: PreAnyOperator<'r, In = In, Final = OperatorType> + fmt::Debug,
    > PreNodePrivate<'r, In, Precedence, OperatorType, PreOperatorType>
{
    pub fn precedence(&self) -> Option<Precedence> {
        match self {
            PreNodePrivate::Nothing
            | PreNodePrivate::Constant(..)
            | PreNodePrivate::Variable(..)
            | PreNodePrivate::BracketIn(..)
            | PreNodePrivate::OperatorReady(..) => None,
            PreNodePrivate::BracketOut(_) => Some(Precedence::ZERO),
            PreNodePrivate::OperatorAwaiting(op) => Some(op.precedence.clone()),
        }
    }
    /// Tries to turn `self` into a complete node which has all its operands
    /// and is part of a computational graph.
    ///
    /// If `self` is [`OperatorAwaiting`](PreNodePrivate::OperatorAwaiting), it
    /// calls [`PreAnyOperator::try_finalize`] which checks if the operator
    /// has all its operands and in this case returns a complete node. If this finalization
    /// is successfull, an [`OperatorReady`](PreNodePrivate::OperatorReady) is returned,
    /// the same [`OperatorAwaiting`](PreNodePrivate::OperatorAwaiting) is returned again.
    ///
    /// If `self` is [`BracketIn`](PreNodePrivate::BracketIn) which has been
    /// set, it returns the inner operator. If the bracket has not been set,
    /// an Error is returned.
    ///
    /// # Panics
    ///
    /// If there is more than one strong reference to the value stored in a
    /// [`BracketIn`](PreNodePrivate::BracketIn). __These should always
    /// stay the only strong reference to their content.__
    fn try_finalize_owned(self) -> Result<Self, Error> {
        Ok(match self {
            PreNodePrivate::Nothing => unreachable!(),
            PreNodePrivate::Constant(k) => PreNodePrivate::Constant(k),
            PreNodePrivate::Variable(name, v) => PreNodePrivate::Variable(name, v),
            PreNodePrivate::BracketIn(op) => Rc::try_unwrap(op)
                .unwrap_or_else(|_| panic!("More than one strong reference to bracket"))
                .into_inner()
                .ok_or_else(|| Error::EmptyBracket)?,
            PreNodePrivate::BracketOut(bra) => PreNodePrivate::BracketOut(bra),
            PreNodePrivate::OperatorReady(op) => PreNodePrivate::OperatorReady(op),
            PreNodePrivate::OperatorAwaiting(op) => match op.ty.try_finalize() {
                Ok(fin) => PreNodePrivate::OperatorReady(fin),
                Err((ty, _)) => PreNodePrivate::OperatorAwaiting(PreOperator {
                    precedence: op.precedence,
                    ty,
                }),
            },
        })
    }
    /// Tries to turn `self` into a complete node which has all its operands
    /// and is part of a computational graph. This is done in-place.
    ///
    /// If `self` is [`OperatorAwaiting`](PreNodePrivate::OperatorAwaiting), it
    /// calls [`PreAnyOperator::try_finalize`] which checks if the operator
    /// has all its operands and in this case returns a complete node. If this finalization
    /// is successfull, an [`OperatorReady`](PreNodePrivate::OperatorReady) is returned,
    /// the same [`OperatorAwaiting`](PreNodePrivate::OperatorAwaiting) is returned again.
    ///
    /// If `self` is [`BracketIn`](PreNodePrivate::BracketIn) which has been
    /// set, it returns the inner operator. If the bracket has not been set,
    /// an Error is returned.
    ///
    /// # Panics
    ///
    /// If there is more than one strong reference to the value stored in a
    /// [`BracketIn`](PreNodePrivate::BracketIn). __These should always
    /// stay the only strong reference to their content.__
    pub fn try_finalize(&mut self) -> Result<(), Error> {
        let res = std::mem::replace(self, PreNodePrivate::Nothing).try_finalize_owned()?;
        *self = res;
        Ok(())
    }
    /// Returns the next node to find process after `self` has been finalized.
    ///
    /// In case of an [`OperatorReady`](PreNodePrivate::OperatorReady), a copy
    /// of the inner node is returned.
    ///
    /// In case of a [`BracketOut`](PreNodePrivate::BracketOut), the node
    /// with which it is connected is returned (in order to step out of the current bracket).
    ///
    /// In all other cases, `None` is returned.
    pub fn into_next_op(&self) -> Option<OperatorType> {
        match self {
            PreNodePrivate::OperatorReady(op) => Some(op.clone()),
            PreNodePrivate::BracketOut(op) => {
                if let Some(Some(PreNodePrivate::OperatorReady(fin))) =
                    op.upgrade().as_deref().map(RefCell::borrow).as_deref()
                {
                    Some(fin.clone())
                } else {
                    None
                }
            }
            // Variables and constants are always leaf nodes,
            // so this function will never be called witht one of them.
            _ => None,
        }
    }
    pub fn unwrap_bracket(self) -> Self {
        match self {
            PreNodePrivate::BracketIn(op) if RefCell::borrow(&op).is_some() => {
                Rc::try_unwrap(op)
                    .unwrap_or_else(|_| panic!("More than one strong reference to bracket"))
                    .into_inner()
                    // Won't panic because inner is some
                    .unwrap()
            },
            this => this
        }
    }
    /// Returns whether the operator is a leaf node, i.e. `Variable`, `Constant`
    /// or `BracketIn`.
    pub fn is_leaf(&self) -> bool {
        match self {
            PreNodePrivate::Nothing => false,
            PreNodePrivate::Constant(..)
            | PreNodePrivate::Variable(..)
            | PreNodePrivate::BracketIn(..) => true,
            PreNodePrivate::BracketOut(..)
            | PreNodePrivate::OperatorReady(..)
            | PreNodePrivate::OperatorAwaiting(..) => false,
        }
    }
    /// Returns whether `self` contains a complete node with all its operands.
    pub fn is_ready(&self) -> bool {
        match self {
            PreNodePrivate::Nothing => false,
            PreNodePrivate::Constant(..)
            | PreNodePrivate::Variable(..)
            | PreNodePrivate::BracketIn(..)
            | PreNodePrivate::OperatorReady(..) => true,
            PreNodePrivate::BracketOut(bra) => bra
                .upgrade()
                .map(|bra| bra.borrow().is_some())
                .unwrap_or(false),
            PreNodePrivate::OperatorAwaiting(..) => false,
        }
    }
    /// Returns whether the node is the out node of a bracket.
    pub fn is_out(&self) -> bool {
        match self {
            PreNodePrivate::Nothing => false,
            PreNodePrivate::BracketOut(..) => true,
            _ => false,
        }
    }
}
