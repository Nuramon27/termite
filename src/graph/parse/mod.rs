use std::collections::HashMap;

use std::cell::RefCell;
use std::cmp::Ordering;
use std::fmt;
use std::ops::{Deref, DerefMut};
use std::rc::Rc;

use super::root::Root;
use crate::nodes::{Variable};
use crate::shared_node::SharedNodeTyped;
use crate::Graph;
use crate::{Side, LEFT, RIGHT};
use custom::*;
use pre_node_private::PreNodePrivate;

pub mod custom;
mod pre_node_private;

/// Parses the term `s` into a graph, using the lexing-function `get_operand`
/// which takes a `str`, returns the node at the beginning of it, and truncates
/// its argument at the start to the end of the operand found.
///
/// `In` is the type of the variables taken by the graph, `Out` is the type
/// of the result of the graph.
///
/// `Precedence` is the type which represents precedences of the operators.
///
/// `OperatorType` is the type of the final operators which have received all their operands,
/// and `PreOperatorType` is basically the type of operators returned `get_operand`
/// (i.e. in the [`PreNode::PreOperator`]-case, but it may as well return variables, constants,
/// and brackets.).
///
/// `get_operand` should return Ok(None) if there is simply no node at the beginning,
/// like one or more spaces, and return an Error if there is something truly wrong with
/// the term.
pub fn parse<
    'a,
    'r,
    In: fmt::Debug + 'r,
    Out: fmt::Debug + 'r,
    Precedence: Fix + fmt::Debug,
    OperatorType: AnyOperator<'r, Out = Out> + fmt::Debug + 'r,
    PreOperatorType: PreAnyOperator<'r, In = In, Final = OperatorType> + fmt::Debug + 'r,
    F: Fn(&mut &str) -> Result<Option<PreNode<Precedence, OperatorType, PreOperatorType>>, Error>,
>(
    mut s: &'a str,
    get_operand: F,
) -> Result<Graph<'r, In, Out>, Error> {
    let mut variables = HashMap::new();
    let mut bracket_stack = Vec::new();
    let mut curr_bra = 0usize;

    let connector = Rc::new(RefCell::new(None));
    let mut brackets = vec![vec![PreNodePrivate::BracketOut(Rc::downgrade(&connector))]];
    while !s.is_empty() {
        if let Some(operand) = get_operand(&mut s)? {
            match operand {
                PreNode::Constant(k) => {
                    brackets[curr_bra].push(PreNodePrivate::Constant(k));
                }
                PreNode::Variable { name } => {
                    let var = variables
                        .entry(name.clone())
                        .or_insert_with(|| SharedNodeTyped::new(Variable::new()));
                    brackets[curr_bra].push(PreNodePrivate::Variable(name, var.clone()));
                }
                PreNode::BracketOpen => {
                    let connector = Rc::new(RefCell::new(None));
                    let connector_weak = Rc::downgrade(&connector);
                    brackets[curr_bra].push(PreNodePrivate::BracketIn(connector));
                    bracket_stack.push(std::mem::replace(&mut curr_bra, brackets.len()));
                    brackets.push(vec![PreNodePrivate::BracketOut(connector_weak)]);
                }
                PreNode::BracketClose => {
                    curr_bra = bracket_stack.pop().ok_or(Error::UnmatchedClosingParen)?;
                }
                PreNode::PreOperator(op) => {
                    brackets[curr_bra].push(PreNodePrivate::OperatorAwaiting(op))
                }
            }
        }
    }

    let mut nodes = Vec::new();
    let mut root = None;

    let mut current_ops = Vec::new();
    let mut next_ops = Vec::new();
    for bracket in brackets.iter_mut().rev() {
        root = None;
        for (i, op) in bracket.iter().enumerate() {
            if op.is_leaf() {
                current_ops.push(i);
            }
        }
        while !current_ops.is_empty() {
            for i in current_ops.drain(..) {
                let (half1, rest) = bracket.split_at_mut(i);
                let (current_node, half2) = rest.split_at_mut(1);
                // May yet panic
                let current_node = current_node.first_mut().unwrap();
                if !current_node.is_out() {
                    let (rec, side, index) =
                        find_receiver::<In, Out, Precedence, OperatorType, PreOperatorType>(
                            current_node.precedence(),
                            half1,
                            half2,
                        )?;
                    match rec {
                        PreNodePrivate::BracketOut(bra) => {
                            let current_node =
                                std::mem::replace(current_node, PreNodePrivate::Nothing).unwrap_bracket();
                            *RefCell::borrow_mut(
                                bra.upgrade()
                                    .expect("Connector destroyed before it could be set")
                                    .deref(),
                            ) = Some(current_node);
                        }
                        PreNodePrivate::OperatorAwaiting(rec) => {
                            edge_with_operator(&mut rec.ty, current_node, side.toggled())?
                        }
                        _ => return Err(Error::WrongReceiver),
                    }
                    rec.try_finalize()?;
                    if let Some(rec_final) = rec.into_next_op() {
                        match rec.is_out() {
                            true => root = Some(rec_final.to_final()?),
                            false => nodes.push(rec_final.to_calc()),
                        }
                        match side {
                            LEFT => next_ops.push(index),
                            RIGHT => next_ops.push(index + half1.len() + 1),
                        }
                    }
                }
            }
            current_ops.clear();
            std::mem::swap(&mut current_ops, &mut next_ops);
        }
    }

    Ok(Graph {
        variables,
        nodes,
        root: Root::new(root.ok_or(Error::NoRootFound)?),
    })
}

/// Sets `operator` as the operand of `rec` at `side`, creating
/// an edge between them.
///
/// # Panics
///
/// Panics if `operator` is an [`OperatorAwaiting`](PreNodePrivate::OperatorAwaiting)
/// a [`BracketOut`](PreNodePrivate::BracketOut)
/// or a [`BracketIn`](PreNodePrivate::BracketIn) containing a `None`.
fn edge_with_operator<
    'r,
    In: 'r,
    Precedence: Fix,
    OperatorType: AnyOperator<'r>,
    PreOperatorType: PreAnyOperator<'r, In = In, Final = OperatorType>,
>(
    rec: &mut PreOperatorType,
    operator: &mut PreNodePrivate<'r, In, Precedence, OperatorType, PreOperatorType>,
    side: Side,
) -> Result<(), Error> {
    match operator {
        PreNodePrivate::Nothing => unreachable!(),
        PreNodePrivate::Constant(k) => rec.edge(k, side),
        PreNodePrivate::Variable(_, v) => {
            rec.edge_with_input(SharedNodeTyped::to_untyped(&v), side)
        }
        PreNodePrivate::BracketIn(op) => {
            let mut inner = RefCell::borrow_mut(op.deref());
            if let Some(mut inner) = inner.deref_mut().as_mut() {
                edge_with_operator(rec, &mut inner, side)
            } else {
                panic!("A bracket not containing a connector cannot get receivers.")
            }
        }
        PreNodePrivate::BracketOut(_) => panic!("Outgoing Brackets do not get receivers."),
        PreNodePrivate::OperatorReady(op) => rec.edge(op, side),
        PreNodePrivate::OperatorAwaiting(_) => panic!("Awaiting operators cannot get receivers."),
    }
}

/// Finds the operator which receives a given node as operand, with this node
/// being between `nodes_left` and `nodes_right` and having `own_precedence`
/// as precedence.
///
/// Returns a reference to the receiver found, the side on which it was found
/// and the index of the receiver in `nodes_left` or `nodes_right` respectively.
///
/// ## Strategy for finding the receiver
///
/// * If the current node is a left-first operator, candidates for the receiver are the next
/// operator with lower precedence to the left or the next operator
/// with lower or equal precedence to the right. If the current node is a right-first operator,
/// “lower” and “lower equal” change places.
///
/// * Of these candidates the one
/// with higher precedence is taken.
///
/// * If both candidates have equal precedence, their `first`-property decides which
/// one is the receiver: The left one for a left-first operator, the right one
/// for a right-first operator.
///
/// In this scheme, leaf nodes like variables and constants are considered
/// to have infinite precedence.
fn find_receiver<
    'a,
    'r,
    In: fmt::Debug + 'r,
    Out: fmt::Debug + 'r,
    Precedence: Fix + fmt::Debug,
    OperatorType: AnyOperator<'r, Out = Out> + fmt::Debug,
    PreOperatorType: PreAnyOperator<'r, In = In, Final = OperatorType> + fmt::Debug,
>(
    own_precedence: Option<Precedence>,
    nodes_left: &'a mut [PreNodePrivate<'r, In, Precedence, OperatorType, PreOperatorType>],
    nodes_right: &'a mut [PreNodePrivate<'r, In, Precedence, OperatorType, PreOperatorType>],
) -> Result<
    (
        &'a mut PreNodePrivate<'r, In, Precedence, OperatorType, PreOperatorType>,
        Side,
        usize,
    ),
    Error,
> {
    let equal_case_left =
        own_precedence
            .as_ref()
            .map(|own_precedence| match own_precedence.first() {
                LEFT => false,
                RIGHT => true,
            });
    let equal_case_right =
        own_precedence
            .as_ref()
            .map(|own_precedence| match own_precedence.first() {
                LEFT => true,
                RIGHT => false,
            });
    let left_candidate = nodes_left.iter_mut().enumerate().rev().find(|(_i, op)| {
        if let Some(precedence) = op.precedence() {
            (match own_precedence
                .as_ref()
                .map(|own_precedence| precedence.cmp(&own_precedence))
                .unwrap_or(Ordering::Less)
            {
                Ordering::Less => true,
                // Won't panic `equal_case_left` is only `None` if `own_precedence` is `None`,
                // but then we land in the `Less` case.
                Ordering::Equal => equal_case_left.unwrap(),
                Ordering::Greater => false,
            } && precedence.position().intersects(Position::PREFIX))
        } else {
            false
        }
    });
    let right_candidate = nodes_right.iter_mut().enumerate().find(|(_i, op)| {
        if let Some(precedence) = op.precedence() {
            (match own_precedence
                .as_ref()
                .map(|own_precedence| precedence.cmp(&own_precedence))
                .unwrap_or(Ordering::Less)
            {
                Ordering::Less => true,
                // Won't panic `equal_case_right` is only `None` if `own_precedence` is `None`,
                // but then we land in the `Less` case.
                Ordering::Equal => equal_case_right.unwrap(),
                Ordering::Greater => false,
            } && precedence.position().intersects(Position::POSTFIX))
        } else {
            false
        }
    });

    match (left_candidate, right_candidate) {
        (
            Some((left_candidate_index, left_candidate)),
            Some((right_candidate_index, right_candidate)),
        ) => Ok(
            match left_candidate
                .precedence()
                .unwrap()
                .cmp(&right_candidate.precedence().unwrap())
            {
                Ordering::Less => (right_candidate, RIGHT, right_candidate_index),
                Ordering::Equal => match left_candidate.precedence().unwrap().first() {
                    LEFT => (left_candidate, LEFT, left_candidate_index),
                    RIGHT => {
                        (right_candidate, RIGHT, right_candidate_index)
                    }
                },
                Ordering::Greater => (left_candidate, LEFT, left_candidate_index),
            },
        ),
        (Some((only_candidate_index, only_candidate)), None) => {
            Ok((only_candidate, LEFT, only_candidate_index))
        }
        (None, Some((only_candidate_index, only_candidate))) => {
            Ok((only_candidate, RIGHT, only_candidate_index))
        }
        (None, None) => Err(Error::NoRecipientFound),
    }
}
