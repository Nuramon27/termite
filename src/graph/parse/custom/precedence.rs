use std::cmp::Ord;

use bitflags::bitflags;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum Side{
    Left,
    Right
}
pub const LEFT: Side = Side::Left;
pub const RIGHT: Side = Side::Right;
impl Side {
    /// Turns a `Left` into `Right` and vice versa.
    #[must_use = "Does not mutate `self`"]
    pub fn toggled(self) -> Self {
        match self {
            Side::Left => Side::Right,
            Side::Right => Side::Left
        }
    }
}

bitflags! {
    /// A type representing the slots where an operator can obtain
    /// operands.
    pub struct Position: u8 {
        /// An operand slot to the right
        const PREFIX = 0x01;
        /// An operand slot to the left
        const POSTFIX = 0x02;
        /// An infix operator
        const INFIX = Self::PREFIX.bits() | Self::POSTFIX.bits();
    }
}

impl From<Side> for Position {
    fn from(other: Side) -> Self {
        match other {
            LEFT => Position::PREFIX,
            RIGHT => Position::POSTFIX,
        }
    }
}

/// A type which can be used as a `Precedence`
/// needs to be ordered and cloneable. Furthermore it needs
/// to support to additional queries which are represented by this trait.
///
/// The [`position`](Fix::position) of the operands and whether
/// operators of equal precedence are evaluated from left to right
/// or from right to left, queried by [`first`](Fix::first).
pub trait Fix: Copy + Ord {
    const ZERO: Self;
    /// The slots where an operand of this Precedence may obtain
    /// operands.
    ///
    /// Operators of the same precedence must take operands
    /// at the same slots for parsing to work, that is why
    /// this is a function on the precedence.
    fn position(&self) -> Position;
    /// The operator which should be evaluated first in the
    /// case that multiple operators with the same precedence
    /// are next to each other.
    ///
    /// Operators of the same precedence must be evaluated
    /// in the same order for parsing to work, that is why
    /// this is a function on the precedence.
    fn first(&self) -> Side;
}
