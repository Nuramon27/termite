use super::Fix;

/// `PreNode` is the type used for lexing a term.
#[derive(Debug)]
pub enum PreNode<Precedence: Fix, OperatorType, PreOperatorType> {
    /// A simple constant.
    Constant(OperatorType),
    /// A variable which can be accessed by its `name`
    /// and thus changed after the graph has been created.
    Variable { name: String },
    /// An opening bracket.
    BracketOpen,
    /// A closing bracket.
    BracketClose,
    /// An operator which yet waits for operands.
    PreOperator(PreOperator<Precedence, PreOperatorType>),
}

/// An operator which yet waits for operands and has a `precedence`.
#[derive(Debug, Clone)]
pub struct PreOperator<Precedence: Fix, PreOperatorType> {
    pub precedence: Precedence,
    pub ty: PreOperatorType,
}
