//! # How to implement your custom tokenizer
//!
//! You will need to Node types: A `PreOperatorType` which holds
//! operators directly after tokenization (awaiting their operands),
//! and an `OperatorType` which holds operators which have all their operands
//! and are ready to be converted into a [`SharedCalc`](crate::shared_node::SharedCalc).
//!
//! These will be passed as the corresponding type parameters to the
//! [`parse`](super::parse) function.
//!
//! Your `PreOperatorType` needs to implement the trait [`PreAnyOperator`]
//! and your `OperatorType` needs to implement the trait [`AnyOperator`].
//!
//! Furthermore you need to provide a `Precedence` type representing
//! operator precedences. These are abstracted by the [`Fix`] trait.

pub mod custom_nodes;
pub mod custom_token;
pub mod precedence;

pub use custom_nodes::{AnyOperator, Multifix, PreAnyOperator};
pub use custom_token::{PreNode, PreOperator};
pub use precedence::{Fix, Position};
use crate::Side;

#[derive(Debug, Clone)]
pub enum Error {
    OperandMissing {
        exp: Side,
    },
    UnexpectedOperand {
        got: Side,
    },
    WrongOperandType {
        got: String,
        exp: String,
        side: Side,
    },
    WrongFinalType {
        got: String,
        exp: String,
    },
    UnmatchedClosingParen,
    EmptyBracket,
    NoRootFound,
    MisplacedOperandForBracket,
    WrongReceiver,
    NoRecipientFound,
    InvalidOperator(String),
}
