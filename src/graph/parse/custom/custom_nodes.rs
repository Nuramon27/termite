use std::ops::DerefMut;

use super::Error;
use crate::nodes::{InfixBuilder, MonadBuilder};
use crate::shared_node::{SharedCalc, SharedNode, SharedNodeTyped};
use crate::{Side, LEFT, RIGHT};

pub trait AnyOperator<'r>: Clone {
    type Out: 'r;
    fn to_calc(&self) -> SharedCalc<'r>;
    fn to_final(&self) -> Result<SharedNode<'r, Self::Out>, Error>;
}

pub trait PreAnyOperator<'r>: Sized {
    type In: 'r;
    type Final: AnyOperator<'r>;
    fn edge(&mut self, parent: &Self::Final, side: Side) -> Result<(), Error>;
    fn edge_with_input(
        &mut self,
        parent: SharedNode<'r, Self::In>,
        side: Side,
    ) -> Result<(), Error>;
    fn try_finalize(self) -> Result<Self::Final, (Self, Error)>;
}

#[derive(Debug)]
pub enum Multifix<'r, In, Out> {
    Prefix {
        operand: Option<SharedNode<'r, In>>,
        node: MonadBuilder<In, Out>,
    },
    Postfix {
        operand: Option<SharedNode<'r, In>>,
        node: MonadBuilder<In, Out>,
    },
    Infix {
        left: Option<SharedNode<'r, In>>,
        right: Option<SharedNode<'r, In>>,
        node: InfixBuilder<In, Out>,
    },
}

impl<'r, In, Out> Multifix<'r, In, Out> {
    pub fn prefix(func: Box<dyn Fn(&In) -> Out>) -> Self {
        Multifix::Prefix {
            operand: None,
            node: MonadBuilder::new(func),
        }
    }
    pub fn postfix(func: Box<dyn Fn(&In) -> Out>) -> Self {
        Multifix::Postfix {
            operand: None,
            node: MonadBuilder::new(func),
        }
    }
    pub fn infix(func: Box<dyn Fn(&In, &In) -> Out>) -> Self {
        Multifix::Infix {
            left: None,
            right: None,
            node: InfixBuilder::new(func),
        }
    }
    pub fn edge(&mut self, parent: SharedNode<'r, In>, side: Side) -> Result<(), Error> {
        match self {
            Multifix::Prefix { operand, node: _ } => {
                if side == RIGHT {
                    *operand = Some(parent);
                } else {
                    return Err(Error::UnexpectedOperand {
                        got: LEFT,
                    });
                }
            }
            Multifix::Postfix { operand, node: _ } => {
                if side == LEFT {
                    *operand = Some(parent);
                } else {
                    return Err(Error::UnexpectedOperand {
                        got: RIGHT,
                    });
                }
            }
            Multifix::Infix {
                left,
                right,
                node: _,
            } => match side {
                LEFT => *left = Some(parent),
                RIGHT => *right = Some(parent),
            },
        }
        Ok(())
    }
    pub fn try_finalize(self) -> Result<SharedNode<'r, Out>, (Self, Error)>
    where
        In: 'r,
        Out: 'r,
    {
        match self {
            Multifix::Prefix {
                operand: Some(operand),
                node,
            } => Ok(SharedNodeTyped::to_untyped(
                &node.with_operands(operand.borrow_mut().deref_mut()),
            )),
            Multifix::Postfix {
                operand: Some(operand),
                node,
            } => Ok(SharedNodeTyped::to_untyped(
                &node.with_operands(operand.borrow_mut().deref_mut()),
            )),
            Multifix::Infix {
                left: Some(left),
                right: Some(right),
                node,
            } => Ok(SharedNodeTyped::to_untyped(
                &node.with_operands(&left, &right),
            )),
            s => match &s {
                Multifix::Prefix { .. } => Err((
                    s,
                    Error::OperandMissing {
                        exp: RIGHT,
                    },
                )),
                Multifix::Postfix { .. } => Err((
                    s,
                    Error::OperandMissing {
                        exp: LEFT,
                    },
                )),
                // todo: which operand is truly missing
                Multifix::Infix { .. } => Err((
                    s,
                    Error::OperandMissing {
                        exp: RIGHT,
                    },
                )),
            },
        }
    }
}
