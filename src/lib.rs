//! # A note on lifetimes.
//!
//! Many items – structs as well as tratis – throughout this crate depend on a
//! lifetime `'r`. This is always used as a lifetime bound on the type(s)
//! used for calculations (e.g. float for arithmetic calculations,
//! `String` for string calculations). In many cases calculations will
//! be done with fully owned values, so `'r` will be `'static`.
//!
//! However termite allows calculations with types containing references, too,
//! and in this case, `'r` will be the lifetime of the shortest-lived
//! reference which appears in any type used for calculations. In that case,
//! the lifetime bounds involving `'r` simply mean that all calculations must
//! be completed and all nodes dropped before the lifetime `'r` ends.use std::fmt;

use std::ops::{Deref, DerefMut};
use std::fmt;

use edge::Edge;
use shared_node::{RefCalc, SharedCalc};

mod edge;
pub mod graph;
pub mod nodes;
pub mod shared_node;

pub use graph::Graph;
//pub use infix::InfixBuilder;
pub use nodes::{Constant, Variable};
pub use graph::parse::custom::Error;
pub use graph::parse::custom::precedence::{Side, LEFT, RIGHT};

/// A general immutable node in a computational graph.
///
/// It can be calculated via [`calc`](Calc::calc), has a list of [`receivers`](Calc::receivers)
/// and can be identified uniquely via its [`id`](Calc::id).
///
/// This trait contains no information about the type of the computation
/// which makes it possible to store `dyn Calc` objects agnostic of their result types.
/// This is important for graphs of mixed types.
pub trait Calc<'r>: fmt::Debug {
    /// An id that is unique across all computational graphs which interact in any way.
    ///
    /// The `id` must not be persistant throughout the live of a node,
    /// i.e. it may change over time. Only two ids which exist at the same
    /// time may be equal only if the corresponding calculations are equivalent.
    fn id(&self) -> usize;
    /// Calculates the node and passes the result on to its [`receivers`](Calc::receivers).
    /// The result of the calculation is never returned (in order to make it possible to
    /// build computational graphs of heavy-to-clone types), but the node
    /// returns whether the computation was successful or not.
    fn calc(&self) -> Result<(), ()>;
    /// The `receivers` of a node are all nodes which take the result of this node
    /// as an input operand.
    ///
    /// This function returns an iterators yielding all those nodes in an arbitrary order.
    fn receivers<'a>(&'a self) -> Box<dyn Iterator<Item = RefCalc<'a, 'r>> + 'a>;
}

/// A conevenience function which turns a slice of receivers into a boxed iterator of
/// borrowed `dyn Calc`s.
fn to_calc_iter<'a, 'r>(v: &'a [SharedCalc<'r>]) -> Box<dyn Iterator<Item = RefCalc<'a, 'r>> + 'a> {
    Box::new(v.iter().map(|r| r.borrow()))
}

/// A helper trait which allows conversion from a `dyn Node` to `dyn Calc`.
pub trait ToCalc<'r> {
    /// Return a reference to oneself in form of a `dyn Calc`.
    fn as_calc<'a>(self: &'a Self) -> &'a (dyn Calc<'r> + 'r);
    /// Return a mutable reference to oneself in form of a `dyn Calc`.
    fn as_calc_mut<'a>(self: &'a mut Self) -> &'a mut (dyn Calc<'r> + 'r);
    /// Convert oneself into a `Box<dyn Calc>`.
    fn into_calc(self: Box<Self>) -> Box<dyn Calc<'r> + 'r>;
}

impl<'r, T: Calc<'r> + 'r> ToCalc<'r> for T {
    fn as_calc<'a>(self: &'a Self) -> &'a (dyn Calc<'r> + 'r) {
        self
    }
    fn as_calc_mut<'a>(self: &'a mut Self) -> &'a mut (dyn Calc<'r> + 'r) {
        self
    }
    fn into_calc(self: Box<Self>) -> Box<dyn Calc<'r> + 'r> {
        self
    }
}

/// A mutable node in a computational graph.
///
/// You can add receivers to a `Node` and obtain a reference to the result of a node.
pub trait Node<'r>: Calc<'r> + ToCalc<'r> {
    /// The result of the `Node`.
    type Res: 'r;
    /// Add `receiver` to the node.
    fn add_receiver(&mut self, receiver: SharedCalc<'r>);
    /// Returns a mutable reference to the [`Edge`] which stores
    /// the result of this `Node`.
    fn target(&self) -> &Edge<Self::Res>;
}

impl<'r, T: Calc<'r> + ?Sized> Calc<'r> for Box<T> {
    fn id(&self) -> usize {
        self.deref().id()
    }
    fn calc(&self) -> Result<(), ()> {
        self.deref().calc()
    }
    fn receivers<'a>(&'a self) -> Box<dyn Iterator<Item = RefCalc<'a, 'r>> + 'a> {
        self.deref().receivers()
    }
}

impl<'r, Res: 'r, T: Node<'r, Res = Res> + ?Sized + 'r> Node<'r> for Box<T> {
    type Res = Res;
    fn add_receiver(&mut self, receiver: SharedCalc<'r>) {
        self.deref_mut().add_receiver(receiver)
    }
    fn target(&self) -> &Edge<Self::Res> {
        self.deref().target()
    }
}

#[macro_export]
macro_rules! apply_to_operand {
    ( $func: expr, $( $arg: expr ),* ) => {
        $func( $( $arg.value.borrow().as_ref().ok_or(())? ),* )
    };
}
